<?php

/**
 * @file
 * Deleted modules drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function deleted_modules_drush_command() {
  $items = array();
  $items['deleted-modules'] = array(
    'description' => 'Shows modules that were deleted but not properly uninstalled.',
    'aliases' => array('dm'),
    'engines' => array(
      'outputformat' => array(
        'default' => 'table',
        'pipe-format' => 'json',
        'field-labels' => array(
          'name' => 'Name',
          'version' => 'Version',
          'status' => 'Status',
          'schema_version' => 'Schema version',
        ),
      ),
    ),
  );

  $items['deleted-modules-cleanup'] = array(
    'description' => 'Downloads, enables, disables and uninstalls modules that were deleted but not properly uninstalled.',
    'aliases' => array('dmc'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function deleted_modules_drush_help($section) {
  switch ($section) {
    case 'drush:deleted-modules':
      return dt("Show modules and projects that were deleted but not properly uninstalled.");

    case 'drush:deleted-modules-cleanup':
      return dt("Attempts to cleanup deleted modules that were not properly uninstalled by downloading, enabling, disabling and uninstalling them.");
  }
}

/**
 * Drush command callback.
 */
function drush_deleted_modules() {
  $deleted_modules = _get_deleted_modules();
  $num_deleted_modules = count($deleted_modules);
  $deleted_modules = _deleted_modules_formatted($deleted_modules);
  if ($num_deleted_modules == 0) {
    drush_log("No deleted projects but not properly uninstalled found.", 'ok');
  }
  else {
    drush_log("{$num_deleted_modules} deleted projects found that were not properly uninstalled:", 'warning');
  }

  return $deleted_modules;
}


/**
 * Drush command callback.
 *
 * @return bool
 *   Whether or not the command was successful.
 */
function drush_deleted_modules_cleanup() {
  $deleted_modules = _get_deleted_modules();
  $num_deleted_modules = count($deleted_modules);
  if ($num_deleted_modules == 0) {
    return "No deleted projects found that were not properly uninstalled.";
  }
  drush_log("{$num_deleted_modules} deleted projects found that were not properly uninstalled.", 'warning');
  $header = array(
    'Name',
    'Version',
    'Status',
    'Schema version',
  );
  $formatted = _deleted_modules_formatted($deleted_modules);
  array_unshift($formatted, $header);
  drush_print_table($formatted);

  drush_print(dt('The modules listed above will be downloaded, enabled, disabled and uninstalled.'));
  drush_print(dt('[WARNING] This action should only be done if you have previously backed up the environment.'));
  if (!drush_confirm(dt('Are you sure?'))) {
    return drush_user_abort();
  }

  // Fail as soon as any project fails.
  foreach ($deleted_modules as $name => $module) {
    // We could try to use $module->info['version'] but contrib modules info
    // files are not reliable enough.
    $version = (isset($module->info['version'])) ? $module->info['version'] : '';
    // We check the version of the module to accept only the following ones:
    // 7.x-1.0 or 7.x-10.05 or 7.x-dev;
    // but not the following ones:
    // 7.x-1.x or 7.x-1.0-dev or 7.x-1.0-ac32b41b (hash by patching).
    if (preg_match("/^7\.x-(\d*\.\d*|dev)$/", $version)) {
      if (drush_invoke('pm-download', array($name . '-' . $version)) === FALSE) {
        return drush_set_error("Unable to download {$name}.");
      }
      if (drush_invoke('pm-enable', [$name]) === FALSE) {
        return drush_set_error("Unable to enable {$name}.");
      }
      if (drush_invoke('pm-disable', [$name]) === FALSE) {
        return drush_set_error("Unable to disable {$name}.");
      }
      if (drush_invoke('pm-uninstall', [$name]) === FALSE) {
        return drush_set_error("Unable to uninstall {$name}.");
      }
    }
    else {
      $message = ($version == '') ?
        "No version of {$name} is found, unable to proceed." :
        "Installed version {$version} of {$name} is not supported, unable to proceed.";
      return drush_set_error($message);
    }
    drush_log("Module {$name} was properly uninstalled and should be now safe to remove from the file system.", 'success');
    $path = drush_extension_get_path($module);
    if (drush_confirm(dt("Do you want to remove the uninstalled {$name} module from {$path}?"))) {
      if (drush_delete_dir($path, TRUE)) {
        drush_log("Module {$name} and its files from {$path} was properly removed from the file system.", 'success');
      }
      else {
        return drush_set_error("Unable to remove {$path} from the filesystem.");
      }
    }
  }
  return TRUE;
}

/**
 * Helper function.
 *
 * @return array
 *   The deleted modules which were not uninstalled.
 */
function _get_deleted_modules() {
  // Get all modules system knows about and not marked uninstalled already.
  $result = db_query("SELECT * FROM {system} WHERE type = 'module' AND schema_version > -1 ORDER BY name ASC");
  $projects = array();
  foreach ($result as $record) {
    $record->info = unserialize($record->info);
    $projects[$record->name] = $record;
  }

  // Get all available modules on the file system.
  $available_modules = drupal_system_listing("/\.module$/", "modules", 'name', 0);
  ksort($available_modules);
  // Profiles are modules on D7.
  $available_profiles = drupal_system_listing("/\.profile/", "profiles", 'name', 0);
  ksort($available_profiles);

  $deleted_modules = array();
  foreach ($projects as $key => $module) {
    if (!array_key_exists($key, $available_modules) && !array_key_exists($key, $available_profiles)) {
      $deleted_modules[$key] = $module;
    }
  }
  return $deleted_modules;
}

/**
 * Helper function to prepare the array for drush_print_table().
 *
 * @param array $deleted_modules
 *   The array of modules to print.
 *
 * @return array
 *   The prepared data ready to be used by drush_print_table().
 */
function _deleted_modules_formatted(array $deleted_modules) {
  $rows = array();
  foreach ($deleted_modules as $key => $module) {
    $rows[$key] = array(
      'name' => $module->name,
      'version' => $module->info['version'],
      'status' => $module->status == 1 ? 'Enabled' : 'Disabled',
      'schema_version' => $module->schema_version,
    );
  }
  return $rows;
}
